package main.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import main.choice.RoomChoice;
import main.choice.FieldChoice;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity(name="groups")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int num;
    private String name;

    @Transient
    private Date yDate = new Date();
    @Transient
    private SimpleDateFormat forM = new SimpleDateFormat("dd.MM.yyyy");

    private String startDate = forM.format(yDate);

    @Enumerated(EnumType.STRING)
    private FieldChoice fieldType;

    @Enumerated(EnumType.STRING)
    private RoomChoice room;

    @OneToMany(mappedBy = "group")
    @JsonManagedReference
    private Set<Student> studentSet = new HashSet<>();

    @OneToOne
    @JoinColumn
    private Trainer montag, dienstag, mittwoch, donnerstag, freitag;

    public Group(){}

    public Group(int num, FieldChoice fieldType, RoomChoice room) {
        this.num = num;
        this.fieldType = fieldType;
        this.room = room;
    }

    private String shortField(){
        if(fieldType.equals(FieldChoice.Softwareentwicklung)){
            return "SW";
        }else if(fieldType.equals(FieldChoice.Netzwerktechnik)){
            return "NT";
        }else if(fieldType.equals(FieldChoice.QualifyingSW)){
            return "QSW";
        }else if(fieldType.equals(FieldChoice.QualifyingNT)){
            return "QNT";
        }else
            return fieldType +"";
    }

    public String getName() {
        return shortField() +" " + num;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public int getNum() {
        //num++;
        return num;
    }
    public void setNum(int num) {
        this.num = num;
    }

    public FieldChoice getFieldType() {
        return fieldType;
    }
    public void setFieldType(FieldChoice fieldType) {
        this.fieldType = fieldType;
    }

    public String getRoom() {
        return room.getRoomName();
    }
    public void setRoom(RoomChoice room) {
        this.room = room;
    }

    public Date getyDate() {
        return yDate;
    }
    public void setyDate(Date xDate) {
        this.yDate = yDate;
    }

    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Set<Student> getStudentSet() {
        return studentSet;
    }
    public void setStudentSet(Set<Student> studentSet) {
        this.studentSet = studentSet;
    }

    public Trainer getMontag() {
        return montag;
    }
    public void setMontag(Trainer montag) {
        this.montag = montag;
    }

    public Trainer getDienstag() {
        return dienstag;
    }
    public void setDienstag(Trainer dienstag) {
        this.dienstag = dienstag;
    }

    public Trainer getMittwoch() {
        return mittwoch;
    }
    public void setMittwoch(Trainer mittwoch) {
        this.mittwoch = mittwoch;
    }

    public Trainer getDonnerstag() {
        return donnerstag;
    }
    public void setDonnerstag(Trainer donnerstag) {
        this.donnerstag = donnerstag;
    }

    public Trainer getFreitag() {
        return freitag;
    }
    public void setFreitag(Trainer freitag) {
        this.freitag = freitag;
    }
}
