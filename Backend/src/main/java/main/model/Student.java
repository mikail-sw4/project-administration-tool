package main.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import main.choice.FieldChoice;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;

    @Transient
    private Date xDate = new Date();
    @Transient
    private SimpleDateFormat forM = new SimpleDateFormat("dd.MM.yyyy");

    private String startDate = forM.format(xDate);

    @Enumerated(EnumType.STRING)
    private FieldChoice fieldType;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "groupId")
    private Group group;

    //private String groupName = group.getName();

    public Student(){}

    public Student(String firstName, String lastName, FieldChoice fieldType) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fieldType = fieldType;
    }

    /*public Student(String firstName, String lastName, String startDate, FieldChoice fieldType, Group group) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.startDate = startDate;
        this.fieldType = fieldType;
        this.group = group;
    }*/

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getxDate() {
        return xDate;
    }
    public void setxDate(Date xDate) {
        this.xDate = xDate;
    }

    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public FieldChoice getFieldType() {
        return fieldType;
    }
    public void setFieldType(FieldChoice fieldType) {
        this.fieldType = fieldType;
    }

    public Group getGroup() {
        return group;
    }
    public void setGroup(Group group) {
        this.group = group;
    }
}
