package main.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import main.choice.SubjectChoice;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
public class Trainer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName, lastName, residence;
    private double salaryBrutto, salaryNetto;

    @Enumerated(EnumType.STRING)
    private SubjectChoice subjectType;

    ArrayList<String> employmentDays = new ArrayList<String>();

    public Trainer(){}

    public Trainer(String firstName, String lastName, String residence, double salaryBrutto, double salaryNetto, SubjectChoice subjectType, ArrayList<String> employmentDays) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.residence = residence;
        this.salaryBrutto = salaryBrutto;
        this.salaryNetto = salaryNetto;
        this.subjectType = subjectType;
        this.employmentDays = employmentDays;
    }

    public double bruttoToNetto(){
        if(salaryBrutto<1099.33){
            return salaryBrutto;
        }else if(salaryBrutto>1099.33 && salaryBrutto<1516){
            return salaryBrutto-(salaryBrutto*0.2);
        }else if(salaryBrutto>1516.00 && salaryBrutto<2599.33){
            return salaryBrutto-(salaryBrutto*0.325);
        }else if(salaryBrutto>2599.33 && salaryBrutto<5016.00){
            return salaryBrutto-(salaryBrutto*0.42);
        }else if(salaryBrutto>5016.00 && salaryBrutto<7516.00){
            return salaryBrutto-(salaryBrutto*0.48);
        }else if(salaryBrutto>7516.00 && salaryBrutto<83349.33){
            return salaryBrutto-(salaryBrutto*0.50);
        }else{
            return salaryBrutto-(salaryBrutto*0.55);
        }
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getResidence() {
        return residence;
    }
    public void setResidence(String residence) {
        this.residence = residence;
    }

    public double getSalaryBrutto() {
        return salaryBrutto;
    }
    public void setSalaryBrutto(double salaryBrutto) {
        this.salaryBrutto = salaryBrutto;
    }

    public double getSalaryNetto() {
        return bruttoToNetto();
    }
    public void setSalaryNetto(double salaryNetto) {
        this.salaryNetto = salaryNetto;
    }

    public SubjectChoice getSubjectType() {
        return subjectType;
    }
    public void setSubjectType(SubjectChoice subjectType) {
        this.subjectType = subjectType;
    }

    public ArrayList<String> getEmploymentDays() {
        return employmentDays;
    }
    public void setEmploymentDays(ArrayList<String> employmentDays) {
        this.employmentDays = employmentDays;
    }
}
