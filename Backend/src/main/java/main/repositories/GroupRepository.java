package main.repositories;

import main.model.Group;
import main.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    public default void editGroup(Group group, long id){
        save(group);
    }
}
