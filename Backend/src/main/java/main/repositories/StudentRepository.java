package main.repositories;

import main.dto.StudentGroupDTO;
import main.model.Student;
import main.model.Trainer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    public default void editStudent(Student student, long id){
        save(student);
    }

}
