package main.repositories;

import main.model.Student;
import main.model.Trainer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainerRepository extends JpaRepository<Trainer, Long> {
    public default void editTrainer(Trainer trainer, long id){
        save(trainer);
    }
}
