package main;

import main.choice.FieldChoice;
import main.choice.RoomChoice;
import main.model.Group;
import main.model.Student;
import main.model.Trainer;
import main.repositories.StudentRepository;
import main.repositories.TrainerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.Date;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class);
    }
}
