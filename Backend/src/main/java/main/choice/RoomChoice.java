package main.choice;

public enum RoomChoice {
    Room1("1, Büro"),
    Room2("2, Büro"),
    Room3("3, Kanrad Zuse"),
    Room4("4, Exchange"),
    Room5("5, Recharge"),
    Room6("6, Cockpit"),
    Room7("7, Grace Hopper"),
    Room8("8, Explorer"),
    Room9("9, Alan Turing"),
    Room10("10, Radia Perlman"),
    Room11("11, Linus Torvalds"),
    Room12("12, Ada Lovelace"),
    Room13("13, James Gosling"),
    Room14("14, Margaret Hamilton");

    private final String roomB;

    RoomChoice(String roomName) {
        this.roomB = roomName;
    }

    public String getRoomName() {
        return roomB;
    }
}
