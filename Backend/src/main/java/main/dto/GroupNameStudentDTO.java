package main.dto;

import main.model.Student;

public class GroupNameStudentDTO {
    private Student student;
    private String groupName;

    public GroupNameStudentDTO(Student student, String groupName) {
        this.student = student;
        this.groupName = groupName;
    }

    public Student getStudent() {
        return student;
    }
    public void setStudent(Student student) {
        this.student = student;
    }

    public String getGroupName() {
        return groupName;
    }
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
