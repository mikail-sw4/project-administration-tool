package main.dto;

import main.model.Student;
import main.model.Trainer;

public class GroupNameTrainerDTO {
    private Trainer trainer;
    private String groupName;

    public GroupNameTrainerDTO(Trainer trainer, String groupName) {
        this.trainer = trainer;
        this.groupName = groupName;
    }

    public Trainer getTrainer() {
        return trainer;
    }
    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public String getGroupName() {
        return groupName;
    }
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
