package main.dto;

import java.util.ArrayList;
import java.util.List;

public class TrainerGroupDTO {
    private long groupId;
    private long trainerId;
    private String lessonDay;

    public TrainerGroupDTO(long groupId, long trainerId, String lessonDay) {
        this.groupId = groupId;
        this.trainerId = trainerId;
        this.lessonDay = lessonDay;
    }

    public long getGroupId() {
        return groupId;
    }
    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getTrainerId() {
        return trainerId;
    }
    public void setTrainerId(long trainerId) {
        this.trainerId = trainerId;
    }

    public String getLessonDay() {
        return lessonDay;
    }
    public void setLessonDay(String lessonDay) {
        this.lessonDay = lessonDay;
    }
}
