package main.dto;

public class StudentGroupDTO {
    private long groupId;
    private long studentId;

    public StudentGroupDTO(long groupId, long studentId) {
        this.groupId = groupId;
        this.studentId = studentId;
    }

    public long getGroupId() {
        return groupId;
    }
    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getStudentId() {
        return studentId;
    }
    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }
}
