package main.controller;

import main.dto.GroupNameStudentDTO;
import main.dto.GroupNameTrainerDTO;
import main.dto.StudentGroupDTO;
import main.dto.TrainerGroupDTO;
import main.model.Group;
import main.model.Student;
import main.model.Trainer;
import main.repositories.GroupRepository;
import main.repositories.StudentRepository;
import main.repositories.TrainerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
public class MyController {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    TrainerRepository trainerRepository;

    @Autowired
    GroupRepository groupRepository;

    //Student
    @GetMapping("/getStudent")
    public List<GroupNameStudentDTO> getStudent(){
        List<GroupNameStudentDTO> studentList = new ArrayList<>();
        for (Student student : studentRepository.findAll()){
            if (student.getGroup()== null){
                studentList.add(new GroupNameStudentDTO(student, ""));
            }
            else{
                studentList.add(new GroupNameStudentDTO(student, groupRepository.findById(student.getGroup().getId()).get().getName()));
            }
        }
        return studentList;
    }

    @PostMapping("/addStudent")
    public void addStudent(@RequestBody Student student){
       studentRepository.save(student);
    }

    @DeleteMapping("/deleteStudent/{id}")
    public void deleteStudent(@PathVariable long id) {
        studentRepository.deleteById(id);
    }

    @PutMapping("/editStudent/{id}")
    public Student editStudent(@RequestBody Student student, @PathVariable long id) {
        studentRepository.editStudent(student, id);
        return student;
    }

    //Trainer
    @GetMapping("/getTrainer")
    public List<Trainer> getTrainer() {
        List<Trainer> trainerList = (List<Trainer>) trainerRepository.findAll();
        return trainerList;
    }

        @PostMapping("/addTrainer")
    public void addTrainer(@RequestBody Trainer trainer){
        trainerRepository.save(trainer);
    }

    @DeleteMapping("/deleteTrainer/{id}")
    public void deleteTrainer(@PathVariable long id) {
        trainerRepository.deleteById(id);
    }

    @PutMapping("/editTrainer/{id}")
    public Trainer editTrainer(@RequestBody Trainer trainer, @PathVariable long id) {
        trainerRepository.editTrainer(trainer, id);
        return trainer;
    }

    //Group
    @GetMapping("/getGroup")
    public List<Group> getGroup(){
        List<Group> groupList = (List<Group>) groupRepository.findAll();
        return groupList;
    }

    @PostMapping("/addGroup")
    public void addGroup(@RequestBody Group group){
        groupRepository.save(group);
    }

    @DeleteMapping("/deleteGroup/{id}")
    public void deleteGroup(@PathVariable long id) {
        groupRepository.deleteById(id);
    }

    @PutMapping("/editGroup/{id}")
    public Group editGroup(@RequestBody Group group, @PathVariable long id) {
        groupRepository.editGroup(group, id);
        return group;
    }


    //Student to Group
    @PutMapping("/addStudentToGroup/{id}")
    public void addStudentToGroup(@RequestBody StudentGroupDTO studentGroupDTO, @PathVariable long id) {
        Group group = groupRepository.findById(id).get();
        studentRepository.findById(studentGroupDTO.getStudentId()).get().setGroup(group);
        studentRepository.save(studentRepository.getById(studentGroupDTO.getStudentId()));

        /*Set<Student> students = new HashSet<>();
        students.add(studentRepository.findById(studentId.getStudentId()).get());
        groupRepository.findById(id).get().setStudentSet(students);
        groupRepository.save(groupRepository.findById(id).get());*/
    }

    //Trainer to Group
    @PutMapping("/addTrainerToGroup/{id}")
    public void addTrainerToGroup(@RequestBody TrainerGroupDTO trainerGroupDTO, @PathVariable long id) {
        if (trainerGroupDTO.getLessonDay().equals("Mo")) {
            groupRepository.findById(id).get().setMontag(trainerRepository.findById(trainerGroupDTO.getTrainerId()).get());
        }
        if (trainerGroupDTO.getLessonDay().equals("Di")) {
            groupRepository.findById(id).get().setDienstag(trainerRepository.findById(trainerGroupDTO.getTrainerId()).get());
        }
        if (trainerGroupDTO.getLessonDay().equals("Mi")) {
            groupRepository.findById(id).get().setMittwoch(trainerRepository.findById(trainerGroupDTO.getTrainerId()).get());
        }
        if (trainerGroupDTO.getLessonDay().equals("Do")) {
            groupRepository.findById(id).get().setDonnerstag(trainerRepository.findById(trainerGroupDTO.getTrainerId()).get());
        }
        if (trainerGroupDTO.getLessonDay().equals("Fr")) {
            groupRepository.findById(id).get().setFreitag(trainerRepository.findById(trainerGroupDTO.getTrainerId()).get());
        }
        groupRepository.save(groupRepository.findById(id).get());
    }
}