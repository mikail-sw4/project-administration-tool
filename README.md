# About me

I'm an administration-tool for educational organizations to organize (CRUD (Create Read Update Delete)) groups, courses, students, teachers and trainers.
Made by Mikail Yagci.

**Used in this Project:**
- **Vue.JS** (HTML/CSS/JavaScript), **SpringBoot** (Java), **MySQL**

# How to work with me

- Mysql or H2
    - To use H2, change the corresponding properties in the `application.properties`-file 
- Install NodeJS & NPM

## Frontend

- Open Project in terminal and run `npm install` and `npm run serve`
- Open `http://localhost:3000` in any browser

## Backend 

- Download Java 17
- Open Project in IntelliJ
- Run defined run-script `Main`
- If everything works correctly, the backend should be available on `http://localhost:8080`


![Landing Page](Frontend/src/assets/LadingPage.png)
![Dashboard](Frontend/src/assets/Dashboard.png)
![CRUD Object](Frontend/src/assets/CRUDObject.png)