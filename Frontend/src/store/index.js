import Vue from 'vue'
import axios from 'axios'
import Vuex from 'vuex'
import VueTilt from 'vue-tilt.js'

Vue.use(Vuex)
Vue.use(VueTilt)



export default new Vuex.Store({
  state: {
    student:[],
    group:[],
    trainer:[],
  },
  getters: {
  },
  mutations: {
    setStudent(state, studentList){
      state.student = studentList;
    },
    setTrainer(state, trainerList){
      state.trainer = trainerList;
    },
    setGroup(state, group){
      state.group = group;
    },
  },
  actions: {
    //Student
    async loadStudent(store){
      const studentResponse = await axios.get('/getStudent')
      store.commit('setStudent', studentResponse.data)
    },
    async addStudent(store, student){
      store = await axios.post('/addStudent', student)
    },
    async deleteStudent(store, id){
      store = await axios.delete('/deleteStudent/' +id)
    },
    async editStudent(store, student) {
      store = await axios.put('/editStudent/' + student.id, student)
    },

    //Trainer
    async loadTrainer(store){
      const trainerResponse = await axios.get('/getTrainer')
      store.commit('setTrainer', trainerResponse.data)
    },
    async addTrainer(store, trainer){
      store = await axios.post('/addTrainer', trainer)
    },
    async deleteTrainer(store, id){
      store = await axios.delete('/deleteTrainer/' +id)
    },
    async editTrainer(store, trainer) {
      store = await axios.put('/editTrainer/' + trainer.id, trainer)
    },

    //Group
    async loadGroup(store){
      const groupResponse = await axios.get('/getGroup')
      store.commit('setGroup', groupResponse.data)
    },
    async addGroup(store, group){
      store = await axios.post('/addGroup', group)
    },
    async deleteGroup(store, id){
      store = await axios.delete('/deleteGroup/' +id)
    },
    async editGroup(store, group) {
      store = await axios.put('/editGroup/' + group.id, group)
    },

    //Student to Group
    async addStudentToGroup(store, studentToGroup) {
      store = await axios.put('/addStudentToGroup/' + studentToGroup.id, studentToGroup)
    },

    //Trainer to Group
    async addTrainerToGroup(store, trainerToGroup) {
      store = await axios.put('/addTrainerToGroup/' + trainerToGroup.id, trainerToGroup)
    },
  },
  modules: {
  }
})
