import Vue from 'vue'
import VueRouter from 'vue-router'
import StudentView from  '../views/StudentView'
import GroupView from  '../views/GroupView'
import TrainerView from '../views/TrainerView'
import LandingView from '../views/LandingView'
import DashboardView from '../views/DashboardView'

Vue.use(VueRouter);

const routes = [
  {
    path: "/teilnehmer", component: StudentView
  },
  {
    path: "/gruppe", component: GroupView
  },
  {
    path: "/trainer", component: TrainerView
  },
  {
    path: "/", component: LandingView, meta:{hideComp: true}
  },
  {
    path: "/dashboard", component: DashboardView, meta:{hideComp: true}
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
